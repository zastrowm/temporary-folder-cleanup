﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace TemporaryFileCleanup
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();

      Hide();
      StartCleaning();
    }

    private async void StartCleaning()
    {
      var cleaner = new Cleaner(new RulesConfiguration(new DirectoryInfo(@"T:\")), new FileSystemService());
      cleaner.DoCleanup();

      while (true)
      {
        await Task.Delay(TimeSpan.FromHours(1));
        cleaner.DoCleanup();
      }
    }

    private void CloseSelf(object sender, ExecutedRoutedEventArgs e)
    {
      Close();
    }
  }

  public class DisplayFileSystemService : IFileSystemService
  {
    public void MoveDirectory(DirectoryInfo directoryToMove, DirectoryInfo newParent)
    {
      Debug.WriteLine($"Moving directory '{directoryToMove}' into '{newParent}'");
    }

    public void MoveFile(FileInfo fileToMove, DirectoryInfo newParent)
    {
      Debug.WriteLine($"Moving file '{fileToMove}' into '{newParent}'");
    }

    public void DeleteDirectory(DirectoryInfo directory)
    {
      Debug.WriteLine($"Deleting directory '{directory}'");
    }

    public void DeleteFile(FileInfo file)
    {
      Debug.WriteLine($"Deleting file '{file}'");
    }

    public void CreateDirectory(DirectoryInfo directory, bool isHidden = false)
    {
      Debug.WriteLine($"Creating directory '{directory}', isHidden: {isHidden}");
    }
  }
}