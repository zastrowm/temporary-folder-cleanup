﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TemporaryFileCleanup
{
  public class RulesConfiguration
  {
    public RulesConfiguration(DirectoryInfo rootDirectory)
    {
      RootDirectory = rootDirectory;

      OldDirectory = FileAbstractionHelpers.GetDirectory(RootDirectory, ".old");

      Rules = new List<DirectoryCleanerRule>()
              {
                new DirectoryCleanerRule(FileAbstractionHelpers.GetDirectory(OldDirectory, ".setup"),
                                         TimeSpan.FromDays(1))
                {
                  Extensions = { ".msi", ".exe" }
                },
                new DirectoryCleanerRule(FileAbstractionHelpers.GetDirectory(OldDirectory, ".archive"),
                                         TimeSpan.FromDays(3))
                {
                  Extensions = { ".zip", ".7z", ".tar", ".gz" }
                }
              };
    }

    public DirectoryInfo RootDirectory { get; }

    public DirectoryInfo OldDirectory { get; }

    public List<DirectoryCleanerRule> Rules { get; }
  }
}