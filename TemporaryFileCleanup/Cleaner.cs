﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TemporaryFileCleanup
{
  /// <summary>
  ///  Moves older files into sub folders and recycles files that are older than a certain amount.
  /// </summary>
  public class Cleaner
  {
    private readonly IFileSystemService _fileSystemService;

    private readonly HashSet<string> _specialFolders;

    public Cleaner(RulesConfiguration rulesConfiguration, IFileSystemService fileSystemService)
    {
      _fileSystemService = fileSystemService;
      _configuration = rulesConfiguration;

      _specialFolders = new HashSet<string>();
      _specialFolders.Add(_configuration.OldDirectory.FullName);

      foreach (var rule in _configuration.Rules)
      {
        _specialFolders.Add(rule.Directory.FullName);
      }

      VerifyFoldersExist();
    }

    public readonly TimeSpan NormalOldTime
      = TimeSpan.FromDays(7);

    public readonly TimeSpan RecycleTime
      = TimeSpan.FromDays(30);

    private readonly RulesConfiguration _configuration;

    private void VerifyFoldersExist()
    {
      if (!_configuration.OldDirectory.Exists)
      {
        _fileSystemService.CreateDirectory(_configuration.OldDirectory, isHidden: true);
      }

      foreach (var rule in _configuration.Rules)
      {
        var directory = rule.Directory;
        if (!directory.Exists)
        {
          _fileSystemService.CreateDirectory(directory);
        }
      }
    }

    public void DoCleanup()
    {
      VerifyFoldersExist();

      MoveOlderFiles();

      RecycleOldEntries();
    }

    private void MoveOlderFiles()
    {
      foreach (var entry in _configuration.RootDirectory.GetFileSystemInfos())
      {
        var fileEntry = entry as FileInfo;
        if (fileEntry != null)
        {
          Process(fileEntry);
        }
        else
        {
          Process((DirectoryInfo)entry);
        }
      }
    }

    private TimeSpan TimeSince(DateTime dateTime)
    {
      return DateTime.UtcNow - dateTime;
    }

    private void Process(FileInfo file)
    {
      if (file.Attributes.HasFlag(FileAttributes.Hidden))
        return;

      var extension = Path.GetExtension(file.Name);

      var timeSinceLastWrite = TimeSince(file.LastWriteTimeUtc);

      foreach (var rule in _configuration.Rules)
      {
        if (!rule.Extensions.Contains(extension))
          continue;

        if (timeSinceLastWrite >= rule.ExpireTime)
        {
          _fileSystemService.MoveFile(file, rule.Directory);
        }

        return;
      }

      // it matched no rules
      if (timeSinceLastWrite > NormalOldTime)
      {
        _fileSystemService.MoveFile(file, _configuration.OldDirectory);
      }
    }

    private void Process(DirectoryInfo directory)
    {
      if (ShouldIgnoreDirectory(directory))
        return;

      if ((DateTime.Now - directory.LastWriteTimeUtc) < NormalOldTime)
        return;

      _fileSystemService.MoveDirectory(directory, _configuration.OldDirectory);
    }

    /// <summary />
    private bool ShouldIgnoreDirectory(DirectoryInfo directory)
    {
      if (directory.Attributes.HasFlag(FileAttributes.System))
        return true;

      return _specialFolders.Any(s => directory.FullName == s);
    }

    private void RecycleOldEntries()
    {
      RecycleChildrenOf(_configuration.OldDirectory);

      foreach (var rule in _configuration.Rules)
      {
        if (!rule.SkipRecycleProcessing)
        {
          RecycleChildrenOf(rule.Directory);
        }
      }
    }

    private void RecycleChildrenOf(DirectoryInfo directory)
    {
      var entries = from entry in directory.GetFileSystemInfos()
                    let lastWrite = TimeSince(entry.LastAccessTimeUtc)
                    where lastWrite > RecycleTime
                    select new
                           {
                             IsDirectory = entry is DirectoryInfo,
                             FileSystemEntry = entry,
                           };

      foreach (var entry in entries)
      {
        if (entry.IsDirectory)
        {
          _fileSystemService.DeleteDirectory((DirectoryInfo)entry.FileSystemEntry);
        }
        else
        {
          _fileSystemService.DeleteFile((FileInfo)entry.FileSystemEntry);
        }
      }
    }
  }
}