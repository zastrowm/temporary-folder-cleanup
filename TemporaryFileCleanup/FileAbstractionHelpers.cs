﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TemporaryFileCleanup
{
  public class FileAbstractionHelpers
  {
    public static FileInfo GetNextFilename(FileInfo file)
    {
      int num = 0;

      string basename = file.FullName.Substring(0, file.FullName.Length - file.Extension.Length);
      string extension = file.Extension;

      while (file.Exists)
      {
        file = new FileInfo(String.Format("{0}__({1})__{2}",
                                          basename,
                                          num++,
                                          extension));
      }

      return file;
    }

    public static DirectoryInfo GetNextDirectoryName(DirectoryInfo directory)
    {
      int num = 0;

      string basename = directory.FullName.Substring(0, directory.FullName.Length);

      while (directory.Exists)
      {
        directory = new DirectoryInfo(String.Format("{0}__({1})__",
                                                    basename,
                                                    num++));
      }

      return directory;
    }

    public static DirectoryInfo GetDirectory(DirectoryInfo root, string path)
      => new DirectoryInfo(Path.Combine(root.FullName, path));
  }
}