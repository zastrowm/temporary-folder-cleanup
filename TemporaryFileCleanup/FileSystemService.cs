﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic.FileIO;

namespace TemporaryFileCleanup
{
  public interface IFileSystemService
  {
    void MoveDirectory(DirectoryInfo directoryToMove, DirectoryInfo newParent);
    void MoveFile(FileInfo fileToMove, DirectoryInfo newParent);
    void DeleteDirectory(DirectoryInfo directory);
    void DeleteFile(FileInfo file);
    void CreateDirectory(DirectoryInfo directory, bool isHidden = false);
  }

  public class FileSystemService : IFileSystemService
  {
    public void MoveDirectory(DirectoryInfo directoryToMove, DirectoryInfo newParent)
    {
      var desiredFileName = new DirectoryInfo(Path.Combine(newParent.FullName, directoryToMove.Name));
      desiredFileName = FileAbstractionHelpers.GetNextDirectoryName(desiredFileName);

      FileSystem.MoveDirectory(directoryToMove.FullName, desiredFileName.FullName, UIOption.OnlyErrorDialogs);
    }

    public void MoveFile(FileInfo fileToMove, DirectoryInfo newParent)
    {
      var desiredFileName = new FileInfo(Path.Combine(newParent.FullName, fileToMove.Name));
      desiredFileName = FileAbstractionHelpers.GetNextFilename(desiredFileName);

      FileSystem.MoveFile(fileToMove.FullName, desiredFileName.FullName, UIOption.OnlyErrorDialogs);
    }

    public void DeleteDirectory(DirectoryInfo directory)
    {
      FileSystem.DeleteDirectory(directory.FullName,
                                 UIOption.OnlyErrorDialogs,
                                 RecycleOption.SendToRecycleBin,
                                 UICancelOption.DoNothing);
    }

    public void DeleteFile(FileInfo file)
    {
      FileSystem.DeleteFile(file.FullName, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
    }

    public void CreateDirectory(DirectoryInfo directory, bool isHidden = false)
    {
      directory.Create();

      if (isHidden)
      {
        directory.Attributes = FileAttributes.Hidden;
      }
    }
  }
}