﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TemporaryFileCleanup
{
  public class DirectoryCleanerRule
  {
    public DirectoryCleanerRule(DirectoryInfo directory, TimeSpan expireTime)
    {
      Directory = directory;
      ExpireTime = expireTime;
      Extensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
    }

    public DirectoryInfo Directory { get; }
    public TimeSpan ExpireTime { get; set; }
    public HashSet<string> Extensions { get; }
    public bool SkipRecycleProcessing { get; set; }
    public bool IsHidden { get; set; }
  }
}